import Vue from 'vue'
import VueRouter from 'vue-router'
import index from '../components/public/index'
import layout from '../components/layouts/index'
import about from '../components/public/about'
import faq from '../components/public/faq'
import contact from '../components/public/contact'
import login from '../components/auth/login'
import forgotPassword from  '../components/auth/forgot_password'
import resetPassword from  '../components/auth/reset_password'
import products from '../components/public/products'
import product from '../components/public/product'
import category from '../components/public/category'
import checkout from '../components/dashboard/checkout'
import cart from '../components/public/cart'
import categories from '../components/public/categories'
import dash_layout from '../components/dashboard/layouts/index'
import dash_index from '../components/dashboard/index'
import profile from '../components/dashboard/profile'

Vue.use(VueRouter)

const routes = [
	{
		path:'', 
		name:'layout', 
		component:layout,
		children:[
			{path:'/', name:'home', component:index},
			{path:'/about', name:'about', component:about},
			{path:'/contact', name:'contact', component:contact},
			{path:'/faq', name:'faq', component:faq},
            {path:'/login', name:'login', component:login},
			{path:'/register', name:'register', component:login},
            {path:'/products', name:'products', component:products},
            {path:'/product/:unique_id', name:'product', component:product},
			{path:'/categories/:url_name', name:'category', component:category},
            {path:'/cart', name:'cart', component:cart},
            {path:'/categories', name:'categories', component:categories},
			//protected route
			{
				path:'/dashboard',
				component:dash_layout,
				children:[
					{path:'/', name:'dashboard', component:dash_index},
					{path:'/profile', name:'profile', component:profile},
					{path:'/checkout', name:'checkout', component:checkout},
				]
			},
		]
	},
    {path:'/forgot-password', name:'forgot-password', component:forgotPassword},
    {path:'/reset-password/:token', name:'reset-password', component:resetPassword},
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})
  
export default router
import axios from 'axios';

export default { 
    handleError(context,error){
        if(error.request.status == 422){
            var resp=JSON.parse(error.request.response);
            var err=resp.errors; 
            var msg='';
            for(var item in err){
                msg=err[item][0];
                break; // it picks the first error ; 
            }  
            context.commit('setNotification',{type:2,msg:msg});
            return msg;
        }else if(error.request.status == 303){
            resp=JSON.parse(error.request.response);
            context.commit('setNotification',{type:2,msg:resp.error}); 
        }
        else if(error.request.status == 404){
            resp=JSON.parse(error.request.response);
            msg= "Request not found";
            context.commit('setNotification',{type:2,msg:msg}); 
        }
        else if(error.request.status == 400){
            resp=JSON.parse(error.request.response);
            msg= resp.message;
            context.commit('setNotification',{type:2,msg:msg}); 
        }
        else if(error.request.status == 401){
            msg="Oops! Authentication error, Please login again";
            context.commit('setNotification',{type:2,msg:msg});
            context.commit('logout');
            // window.location.href="/login";
        } 
        else {
            msg="Oops! server error, Please try again";
            context.commit('setNotification',{type:2,msg:msg});
        }
    },
    post(context , data){  
        return new Promise((resolve,reject)=>{
            axios.post(context.state.endpoint+data.endpoint , data.details,{
                headers: {
                    "Authorization": 'Bearer ' + context.state.token, 
                }
            })
            .then((data)=>{
                resolve(data);
            })
            .catch((error)=>{
                context.dispatch('handleError',error);
                reject(error)
            })
        });
    },
    get(context, endpoint){ 
        return new Promise((resolve,reject)=>{
            axios.get(context.state.endpoint+endpoint,{
                headers: {
                    "Authorization": 'Bearer ' + context.state.token,
                }
            })
            .then((data)=>{
                // console.log(data)
                resolve(data)
            })
            .catch((error)=>{
                console.log('error', error.request); 
                context.dispatch('handleError',error);
                reject(error)
            })
        });
    },
    getUser(state){
        var data=localStorage.getItem('guardmetrics');    
        data=decodeURIComponent(data); 
        data=JSON.parse(data);
        state.commit('setUser', data)
        // console.log(data)
    },
    getDashboard(context){
        return new Promise((resolve,reject)=>{
            axios.get(context.state.endpoint+'get-dashboard',{
                headers: {
                    "Authorization": 'Bearer ' + context.state.token,
                }
            })
            .then((data)=>{
                // console.log(data.data.status);
                if(data.data.status){
                    var result=data.data.data
                    result.token=context.state.token
                    context.commit('setUser',result)
                }else{
                    context.commit('setNotification', {type:2, msg:data.data.msg})
                    console.log('logout')
                    context.commit('logout')
                    console.log('logout 2')
                }
                resolve(data)
            })
            .catch((error)=>{
                context.dispatch('handleError',error);
                reject(error)
            })
        });
    },
}
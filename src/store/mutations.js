export default {
    setNotification(state,data){

        state.notification.type=data.type;
        state.notification.message=data.msg;   
        if(!data.unset){
            setTimeout(()=>{
                state.notification.type=0;
                state.notification.message='';
            },8000);
        }
    },
    getUser(state){
        var data=localStorage.getItem('guardmetrics');    
        data=decodeURIComponent(data); 
        data=JSON.parse(data);
        
        state.user=data;
        state.token=data.token;
        state.logged_in = true
        console.log(state)
    },
    setUser(state,data){ 
        state.user=data;
        state.token=data.token;
        state.logged_in = true

        var result=encodeURIComponent(JSON.stringify(data));
        localStorage.setItem('guardmetrics',result);
    },
    logout(state){
        state.user=null
        state.token = null
        state.logged_in = false
        window.localStorage.removeItem('guardmetrics');
        window.location.href="/login";
    },
    addToCart(state, data){
        console.log(data)
        var stg=window.localStorage.getItem('guardcart');
        stg = JSON.parse(decodeURIComponent(stg))
        state.cart = stg
        var exist = false
        if(!state.cart){
            console.log('not')
            state.cart = [data];
        }else{
            state.cart.forEach(element => {
                if(element.unique_id == data.unique_id){
                    exist = true
                    element.quantity += 1
                    element.colors = data.colors
                }
                         
            });
            if(!exist){
                state.cart.push(data);
            } 
        }
          
        // console.log(stg)
        var result=encodeURIComponent(JSON.stringify(state.cart));
        localStorage.setItem('guardcart',result);   
    },
    setStateCart(state, data){
        state.cart = data
        console.log(state.cart)
    }

}
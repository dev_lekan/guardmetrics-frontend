import Vue from 'vue'
import Vuex from 'vuex'

import getters from './getters'
import mutations from './mutations'
import actions from './actions'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    endpoint:process.env.VUE_APP_ENDPOINT, 
    logged_in:false,
    notification: {
      type:0,
      msg: ''
    },
    token:'',
    user:null,
    cart:[],
  },
  mutations,
  actions,
  getters,
})

export default store
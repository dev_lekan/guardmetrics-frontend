import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import store from './store'
import Vuex from 'vuex'
import notification from './components/layouts/notification'
import loader from '@/components/layouts/loader'


Vue.use(Vuex);
Vue.component('notification',notification);
Vue.component('loader',loader);

        //plugin can be use to create a global method in vue
const Plugin = {
  install(Vue) {
    Vue.prototype.addToCartPlugin = (item) => {
      store.commit('addToCart', {
        unique_id:item.unique_id,
        name:item.name,
        image:item.images[0].name,
        quantity:1,
        price:item.price,
        colors:[],
      })
    }
  },
}
Vue.use(Plugin)

Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
